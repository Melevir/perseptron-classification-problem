#!/usr/bin/python
#coding: utf-8

# Solver of classification linear problem with perceptron network.

from itertools import cycle
import unittest

import matplotlib
import matplotlib.pyplot as plt

from tests.test_perceptron import *
import utils 

        
def restore_settings():
    settings = utils.load_json_config('config/perceptron.cfg')
    return settings


def init_data(settings):
    weights = []
    for i in xrange(settings['classes_amount']):
        weights.append([settings['weight_init_value'] for j in xrange(settings['values_dimension'] + 1)])
    for v in settings['learn_data_set']:
        v['value'].append(settings['fake_value_to_append'])
    return weights


def start_learning(weights, learn_data_set):
    iterations_counter = 0
    while not make_iteration(weights, learn_data_set):
        iterations_counter += 1
    print u"Total {0} iterations.".format(iterations_counter)
    return weights


def make_iteration(weights, learn_data):
    no_functions_updates = True
    for x in learn_data:
        current_weights = []
        for w in weights:
            current_weights.append(utils.scalar_multiplication(w, x['value']))
        init_position = 0 if x['class'] != 0 else 1
        max_d = current_weights[init_position]
        class_to_decrease = init_position
        for i, v in enumerate(current_weights):
            if v > max_d and i != x['class']:
                max_d = v
                class_to_decrease = i
        if max_d >= current_weights[x['class']]:
            no_functions_updates = False
            class_to_increase = x['class']
            print 'Increasing class {0}'.format(class_to_increase)
            weights[class_to_increase] = update_single_division_function(
                weights[class_to_increase], settings['learn_rate'], x, True)
            weights[class_to_decrease] = update_single_division_function(
                weights[class_to_decrease], settings['learn_rate'], x)
    print 'Weights on iteration end: {0}.'.format(weights)
    return no_functions_updates


def update_single_division_function(function, const, learn_data, increase = False):
    increase_const = 1 if increase else -1
    summand = [increase_const * const * v for v in learn_data['value']]
    return [v + summand[i] for i,v in enumerate(function)]


def calc_border_value(weights, x):
    return 0 - (weights[0] * x + weights[2]) / weights[1]


def plot_results(settings, learn_results):
    if settings['values_dimension'] != 2:
        return
    COLORS = 'bgrcmyk'
    colors = cycle(COLORS)
    dataset_pts = plt.figure()
    for i in xrange(settings['classes_amount']):
        pts = filter(lambda p: p['class'] == i, settings['learn_data_set'])
        x_vals = [p['value'][0] for p in pts]
        y_vals = [p['value'][1] for p in pts]
        pts_subplot = dataset_pts.add_subplot(111)
        pts_subplot.plot(x_vals, y_vals, '%so'%colors.next())
    xvals = xmin, xmax = pts_subplot.get_xlim()
    for weights in learn_results:
        border_vals = (calc_border_value(weights, xmin), calc_border_value(weights, xmax))
        border_subplot = dataset_pts.add_subplot(111)
        border_subplot.plot(xvals, border_vals)
    border_subplot.margins(0.1,0.1)
    border_subplot.grid(True)
    plt.show()

    
if __name__ == '__main__':
    if utils.run_tests(PreceptronTestCase):
        settings = restore_settings()
        weights = init_data(settings)
        learn_result = start_learning(weights, settings['learn_data_set'])
        plot_results(settings, learn_result)
