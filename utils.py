import unittest
import json


def scalar_multiplication(v1, v2):
    return reduce(lambda s, v: s + v, [v1[i] * v2[i] for i in xrange(len(v1))], 0)

def run_tests(testcase):
    suite = unittest.TestLoader().loadTestsFromTestCase(testcase)
    return unittest.TextTestRunner(verbosity=0).run(suite).wasSuccessful()

def load_json_config(filename):
    json_data=open(filename)
    data = json.load(json_data)
    json_data.close()
    return data
