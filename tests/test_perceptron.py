#coding: utf-8
import unittest

from perceptron import *


class PreceptronTestCase(unittest.TestCase):
    def test_load_settings(self):
        settings = restore_settings()
        values_dimension = settings.get('values_dimension', None)
        classes_amount = settings.get('classes_amount', None)
        fake_value_to_append = settings.get('fake_value_to_append', None)
        learn_rate = settings.get('learn_rate', None)
        weight_init_value = settings.get('weight_init_value', None)
        learn_data_set = settings.get('learn_data_set', None)
        readed_settings = (values_dimension, classes_amount, fake_value_to_append,
                           learn_rate, weight_init_value, learn_data_set)
        self.assertFalse(None in readed_settings)

    def test_init_data(self):
        settings = restore_settings()
        old_data_size = len(settings['learn_data_set'][0]['value'])
        weights = init_data(settings)
        new_data_size = len(settings['learn_data_set'][0]['value'])
        self.assertEqual(new_data_size - old_data_size, 1)
        self.assertEqual(len(weights), settings['classes_amount'])
