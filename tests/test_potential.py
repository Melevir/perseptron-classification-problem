#coding: utf-8
import unittest

from potential import *


class PotentialClassificatorTestCase(unittest.TestCase):
    def test_load_settings(self):
        settings = restore_settings()
        values_dimension = settings.get('values_dimension', None)
        metric_function_type = settings.get('metric_function_type', None)
        points_side_amount = settings.get('points_side_amount', None)
        learn_data_set = settings.get('learn_data_set', None)
        readed_settings = (values_dimension, metric_function_type, points_side_amount, learn_data_set)
        self.assertFalse(None in readed_settings)

    def test_correct_potential_value(self):
        x = [1,2]
        expected_potential_val = -0.5
        K = list()
        K.append({'multiplicator': -1,'x_vector': [1, 1]})
        metric_function = lambda x, y: 1. / (1 + norm_of_difference_of_vectors(x, y))
        potential_val = calculate_potential_value(K, x, metric_function)
        self.assertEqual(expected_potential_val, potential_val)

    def test_creation_of_test_set(self):
        settings = restore_settings()
        K = list()
        K.append({'multiplicator': -1,'x_vector': [1, 1]})
        testset = work_on_test_set(settings, K)
        points_amount = len(testset[0]) + len(testset[1])
        expected_points_amount = settings['points_side_amount'] ** 2
        self.assertEqual(len(testset), 2)
        self.assertEqual(points_amount, expected_points_amount)
