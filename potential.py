#!/usr/bin/python
#coding: utf-8

# Solver of classification nonlinear problem with method of potential.
# Works for two classes.

from itertools import cycle
import unittest

import matplotlib
import matplotlib.pyplot as plt

import utils
from tests.test_potential import *


def restore_settings():
    METRIC_FUNCTIONS = {
        'fractial' : lambda x, y: 1. / (1 + norm_of_difference_of_vectors(x, y)),
    }
    settings = utils.load_json_config('config/potential.cfg')
    settings['metric_function'] = METRIC_FUNCTIONS[settings['metric_function_type']]
    return settings


def calculate_potential_value(K, x_k, metric_function):
    return reduce(lambda s, v: s + v,
                  [k_elem['multiplicator'] * metric_function(k_elem['x_vector'], x_k) for k_elem in K], 0)


def get_point_type(K, point, metric_function):
    val = calculate_potential_value(K, point, metric_function)
    return 'pos' if val >= 0 else 'neg'


def norm_of_difference_of_vectors(x,y):
    return reduce(lambda s, v: s + v, [(x[i] - y[i]) ** 2 for i in xrange(len(x))])

    
def start_learning(K, learn_data_set, metric_function):
    iterations_counter = 0
    while make_iteration(K, learn_data_set, metric_function):
        iterations_counter += 1
    print u"Total {0} iterations.".format(iterations_counter)
    return K


def make_iteration(K, learn_data, metric_function):
    k_fixed = False
    for item in learn_data:
        if not K:
            val = metric_function(item['value'], [0 for i in xrange(len(item['value']))])
        else:
            val = calculate_potential_value(K, item['value'], metric_function)
        if val <= 0 and item['class'] == 'pos':
            print u'Adding positive part'
            K.append({'multiplicator': 1, 'x_vector': item['value']})
            k_fixed = True
        elif val > 0 and item['class'] == 'neg':
            print u'Adding negative part'
            K.append({'multiplicator': -1, 'x_vector': item['value']})
            k_fixed = True
    return k_fixed


def plot_results(settings, K, testset):
    if settings['values_dimension'] != 2:
        return
    COLORS = 'bg'
    colors = cycle(COLORS)
    dataset_pts = plt.figure()
    for i in xrange(2):
        pts = filter(lambda p: p['class'] == ['pos','neg'][i], settings['learn_data_set'])
        x_vals = [p['value'][0] for p in pts]
        y_vals = [p['value'][1] for p in pts]
        pts_subplot = dataset_pts.add_subplot(111)
        pts_subplot.plot(x_vals, y_vals, '%so'%colors.next())
    pts_subplot = dataset_pts.add_subplot(111)
    for i, points_set in enumerate(testset):
        x_vals = [p[0] for p in points_set]
        y_vals = [p[1] for p in points_set]    
        pts_subplot.plot(x_vals, y_vals, '%sx'%('rc'[i]))
    pts_subplot.margins(0.1,0.1)
    pts_subplot.grid(True)
    plt.show()


def get_2d_points_limits(points):
    xmax = max([p['value'][0] for p in points])
    xmin = min([p['value'][0] for p in points])
    ymax = max([p['value'][1] for p in points])
    ymin = min([p['value'][1] for p in points])
    return xmax, xmin, ymax, ymin


def work_on_test_set(settings, K):
    points_side_amount = settings['points_side_amount']
    xmax, xmin, ymax, ymin = get_2d_points_limits(settings['learn_data_set'])
    h_x = (xmax - xmin * 1.) / (points_side_amount - 1)
    h_y = (ymax - ymin * 1.) / (points_side_amount - 1)
    sorted_points = {'pos' : list(), 'neg' : list()}
    for i in xrange(points_side_amount):
        for j in xrange(points_side_amount):
            point = [xmin + i * h_x, ymin + j * h_y]
            point_type = get_point_type(K, point, settings['metric_function'])
            sorted_points[point_type].append(point)
    return sorted_points.values()


if __name__ == '__main__':
    if utils.run_tests(PotentialClassificatorTestCase):
        settings = restore_settings()
        K = start_learning(list(), settings['learn_data_set'], settings['metric_function'])
        testset = work_on_test_set(settings, K)
        plot_results(settings, K, testset)
